package LessonTasks.Lesson10.Task1;

import java.util.Random;

/**
 * Created by User-PC on 06.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		StringBuilder rnString = new StringBuilder();
		Random rn = new Random();
		int rnStringLenght = 1000;
		int sbConcatInterations = 1000;

		for (int i = 0; i < rnStringLenght; i++) {
			rnString.append((char) (rn.nextInt(26) + 97));
		}

		String s1 = rnString.toString();
		long sbTime = 0;
		long concatTime = 0;
		String s2 = "";
		String s3 = "";
		StringBuilder sb = new StringBuilder();

		concatTime = System.currentTimeMillis();
		for (int i = 0; i < sbConcatInterations + 1; i++) {
			s2 = s2.concat(s1);
		}
		concatTime = System.currentTimeMillis() - concatTime;


		sbTime = System.currentTimeMillis();
		for (int i = 0; i < sbConcatInterations; i++) {
			sb.append(s1);
		}
		s3 = sb.toString();
		sbTime = System.currentTimeMillis() - sbTime;

		System.out.println("Concat: " + concatTime + " StringBuilder: " + sbTime);
	}
}
