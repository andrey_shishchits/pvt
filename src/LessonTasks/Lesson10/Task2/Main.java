package LessonTasks.Lesson10.Task2;

import util.UserInput;

/**
 * Created by User-PC on 06.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		String s1 = UserInput.inputString("Enter string: ");

		s1 = replaceSmile(s1);
		System.out.println(s1);
	}

	public static String replaceSmile(String s) {
		char[] string = s.toCharArray();

		for (int i = 0; i < string.length; i++) {
			if (i + 1 < string.length) {
				if (string[i] == ':' && string[i + 1] == '(')
				{
					string[i + 1] = ')';
				}
			}
		}

		return new String(string);
	}
}
