package LessonTasks.Lesson10.Task3;

import util.UserInput;

/**
 * Created by User-PC on 06.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		String s1 = UserInput.inputString("Enter string: ");
		String word = UserInput.inputString("Enter word: ");

		System.out.println(isStartsAndFinishesWithWord(s1, word));
	}

	private static boolean isStartsAndFinishesWithWord(String s1, String word) {


		return   s1.indexOf(word) == 0 && s1.length() - word.length() == s1.indexOf(word,(s1.length()-word.length())) ;

	}


}
