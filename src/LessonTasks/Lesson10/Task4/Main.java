package LessonTasks.Lesson10.Task4;

import util.UserInput;

/**
 * Created by User-PC on 06.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		String firstName = UserInput.inputString("Enter first name: ");
		String lastName = UserInput.inputString("Enter last name: ");
		String patronymic = UserInput.inputString("Enter patronymic: ");

		System.out.println(getFIO(firstName, lastName, patronymic));

	}

	private static String getFIO(String firstName, String lastName, String patronymic) {
		StringBuilder sb = new StringBuilder();

		sb.append(lastName.toUpperCase().charAt(0))
				.append(". ")
				.append(firstName.toUpperCase().charAt(0))
				.append(". ")
				.append(patronymic.toUpperCase().charAt(0))
				.append(". ");


		return sb.toString();
	}
}
