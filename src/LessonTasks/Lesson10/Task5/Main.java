package LessonTasks.Lesson10.Task5;

import util.UserInput;

/**
 * Created by User-PC on 06.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		String str = UserInput.inputString("Enter line: ");

		System.out.println("There is " + countWords(str) + " words.");
	}

	private static int countWords(String str) {
	 	return str.replaceAll("\\s+", " ").split(" ").length;
	}

}
