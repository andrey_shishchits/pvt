package LessonTasks.Lesson13.Task4;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User-PC on 13.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		String s = "dawdawda  +375336027496  dawdawdaw  +375296028496  ";

		s = phoneNumberFormatter(s);
		System.out.println(s);
	}

	public static String phoneNumberFormatter(String s){
		StringBuilder stringBuilder = new StringBuilder(s);
		Pattern pattern = Pattern.compile("(\\+375)(\\d{2})\\d{7}");
		Matcher matcher = pattern.matcher(stringBuilder);

		while (matcher.find()){
			String substring = stringBuilder.substring(matcher.start(2), matcher.end(2));
			stringBuilder.delete(matcher.start(2), matcher.end(2));
			stringBuilder.insert(matcher.start(2), "(" + substring + ")");

		}

		return stringBuilder.toString();
	}
}
