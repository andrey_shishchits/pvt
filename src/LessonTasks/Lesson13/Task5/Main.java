package LessonTasks.Lesson13.Task5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User-PC on 15.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		String s = "192.168.0.1";
		System.out.println(isIpAdress(s));

	}

	private static boolean isIpAdress(String s) {
		Pattern pattern = Pattern.compile("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$");
		Matcher matcher = pattern.matcher(s);
		int i = 1;

		while (matcher.find()) {
			for (; i < matcher.groupCount(); i++) {
				if (Integer.parseInt(matcher.group(i)) > 255 || Integer.parseInt(matcher.group(i)) < 0) break;
			}
		}
		if (i == 4) {
			return true;
		} else return false;
	}
}
