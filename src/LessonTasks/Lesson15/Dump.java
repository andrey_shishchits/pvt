package LessonTasks.Lesson15;

import java.util.Random;
import java.util.Stack;

/**
 * Created by User-PC on 18.07.2017.
 */
public class Dump {
	private Stack<SpareParts> parts = new Stack<>();

	public Dump() {

	}

	public void pushSparePart(SpareParts sparePart) {
		parts.push(sparePart);
	}

	public SpareParts popSparePart() {
		return parts.pop();
	}

	public boolean isEmpty() {
		return parts.empty();
	}
}
