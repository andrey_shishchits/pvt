package LessonTasks.Lesson15;

import java.util.Random;

public final class Factory {
	private static Random rn;

	static {
		rn = new Random();
	}

	public static void initiateDump(Dump dump){
		for (int i = 0; i < 20; i++) {
			pushNewPart(dump);
		}
	}

	public static void pushNewDay(Dump dump) {
		int todayIllPickThisMuch = rn.nextInt(4) + 1;

		for (int i = 0; i < todayIllPickThisMuch; i++) {
			pushNewPart(dump);
		}
	}

	private static void pushNewPart(Dump dump) {
		switch (rn.nextInt(9) + 1) {
			case 1:
				dump.pushSparePart(SpareParts.HEAD);
				break;
			case 2:
				dump.pushSparePart(SpareParts.BODY);
				break;
			case 3:
				dump.pushSparePart(SpareParts.LEFT_ARM);
				break;
			case 4:
				dump.pushSparePart(SpareParts.RIGHT_ARM);
				break;
			case 5:
				dump.pushSparePart(SpareParts.LEFT_LEG);
				break;
			case 6:
				dump.pushSparePart(SpareParts.RIGHT_LEG);
				break;
			case 7:
				dump.pushSparePart(SpareParts.CPU);
				break;
			case 8:
				dump.pushSparePart(SpareParts.RAM);
				break;
			case 9:
				dump.pushSparePart(SpareParts.HDD);
				break;
		}
	}
}
