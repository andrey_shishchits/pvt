package LessonTasks.Lesson15;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by User-PC on 18.07.2017.
 */
@Data
public class MadScientist extends Thread {
	private ArrayList<SpareParts> sparePartsList = new ArrayList<>();
	private Dump dump;

	public MadScientist(Dump dump){
		this.dump = dump;
	}


	@Override
	public void run() {
		for (int i = 0; i < 100; i++){
			bringNewSparePartsMUHAHAHAHAH();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		System.out.println("I have assembled: " + assembleRobotsMUAHAHAHHAH() + " in Thread" + Thread.currentThread().getName());
	}

	private void bringNewSparePartsMUHAHAHAHAH() {
		Random rn = new Random();
		int todayIllPickThisMuch = rn.nextInt(4) + 1;

		for (int i = 0; i < todayIllPickThisMuch; i++) {
			if (!dump.isEmpty()) {
				sparePartsList.add(dump.popSparePart());
			} else System.out.println("Dump is empty!");
		}
	}

	private int assembleRobotsMUAHAHAHHAH(){
		Map<SpareParts,Integer> map = new HashMap<>();

		sparePartsList.stream().forEach((sp) -> map.put(sp, 0));

		for (SpareParts sp: sparePartsList){
			if (map.containsKey(sp)) {
				map.replace(sp,(map.get(sp)+1));
			}
		}
		System.out.println(map.toString());
		return new ArrayList<Integer>(map.values()).stream().min((p1,p2) -> p1.compareTo(p2)).get();
	}
}
