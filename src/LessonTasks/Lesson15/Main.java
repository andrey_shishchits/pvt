package LessonTasks.Lesson15;

import java.util.Arrays;

/**
 * Created by User-PC on 18.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		Dump dump = new Dump();

		Factory.initiateDump(dump);

		MadScientist madScientist1 = new MadScientist(dump);
		MadScientist madScientist2 = new MadScientist(dump);

		new Thread() {
			public void run(){
				for (int i = 0; i < 100; i++){
					Factory.pushNewDay(dump);
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();

		madScientist1.start();
		madScientist2.start();

	}
}
