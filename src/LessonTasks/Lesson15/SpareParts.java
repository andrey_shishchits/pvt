package LessonTasks.Lesson15;

/**
 * Created by User-PC on 18.07.2017.
 */
public enum SpareParts {
	HEAD,
	BODY,
	LEFT_ARM,
	RIGHT_ARM,
	LEFT_LEG,
	RIGHT_LEG,
	CPU,
	RAM,
	HDD
}
