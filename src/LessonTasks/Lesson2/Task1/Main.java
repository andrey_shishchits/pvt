package LessonTasks.Lesson2.Task1;

import util.UserInput;

/**
 * Created by User-PC on 30.06.2017.
 */
public class Main {
	public static void main(String[] args) {
		String word1 = UserInput.inputString("Enter first word: ");
		String word2 = UserInput.inputString("Enter second word: ");

		System.out.println(StringUtil.stringComparator(word1, word2));

	}
}
