package LessonTasks.Lesson2.Task1;

/**
 * Created by User-PC on 30.06.2017.
 */
public final class StringUtil {
	public static String stringComparator(String s1, String s2) {
		String msg;

		if (s1.equals(s2)) {
			return "Отлично! Слова одинаковы.";
		} else if (s1.equalsIgnoreCase(s2)) {
			return "Хорошо. Почти одинаковы.";
		} else if (s1.length() == s2.length()) return "Ну, хотя бы одной длинны.";
		return "";
	}
}
