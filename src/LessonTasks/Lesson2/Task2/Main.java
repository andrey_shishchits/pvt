package LessonTasks.Lesson2.Task2;

import util.UserInput;

/**
 * Created by User-PC on 01.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		int number = UserInput.inputInt("Enter number of rubles: ");

		RubleUtil.rubleMsgUsingIf(number);
		RubleUtil.rubleMsgUsingSwitch(number);

	}
}
