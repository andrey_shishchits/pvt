package LessonTasks.Lesson2.Task2;

/**
 * Created by User-PC on 01.07.2017.
 */
public final class RubleUtil {
	public static void rubleMsgUsingIf(int number) {
		int lastValue = number % 10;
		String ruble;

		if (lastValue == 1) {
			ruble = "рубль";
		} else if (lastValue == 2 || lastValue == 3 || lastValue == 4) {
			ruble = "рубля";
		} else ruble = "рублей";
		System.out.println(number + " " + ruble);
	}

	public static void rubleMsgUsingSwitch(int number) {
		int lastValue = number % 10;
		String ruble;

		switch (lastValue){
			case 1:
				ruble = "рубль";
				break;
			case 2:
			case 3:
			case 4:
				ruble = "рубля";
				break;
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 0:
				ruble = "рублей";
				break;
			default:
				ruble = "Вот не должно сюда попасть.";
				break;
		}
		System.out.println(number + " " + ruble);
	}
}
