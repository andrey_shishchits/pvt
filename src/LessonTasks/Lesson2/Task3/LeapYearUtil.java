package LessonTasks.Lesson2.Task3;

/**
 * Created by User-PC on 01.07.2017.
 */
public final class LeapYearUtil {
	public static boolean isLeapYear(int number) {
		boolean leapYear = true;
		if (number % 4 != 0) {
			leapYear = false;
		} else if (number % 400 == 0) {
			leapYear = true;
		} else if (number % 100 == 0) leapYear = false;

		return leapYear;
	}
}
