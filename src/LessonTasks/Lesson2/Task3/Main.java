package LessonTasks.Lesson2.Task3;

import util.UserInput;

/**
 * Created by User-PC on 01.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		int year = UserInput.inputInt("Enter year: ");
		String msg;
		if (LeapYearUtil.isLeapYear(year)) {
			msg = "";
		} else msg = "not";

		System.out.println("This is " + msg + " leap-year.");
	}
}
