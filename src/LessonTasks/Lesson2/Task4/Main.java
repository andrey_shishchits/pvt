package LessonTasks.Lesson2.Task4;

import util.UserInput;

/**
 * Created by User-PC on 01.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		int days = UserInput.inputInt("Enter number of days: ");
		int months = UserInput.inputInt("Enter number of months: ");
		int years = UserInput.inputInt("Enter number of years: ");
		String msg;
		if (RealDateUtil.isRealDate(days, months, years)) {
			msg = " real ";
		} else msg = " unreal ";

		System.out.println("It is" + msg + "date: " + days + " " + months + " " + years);
	}

}
