package LessonTasks.Lesson2.Task4;

import LessonTasks.Lesson2.Task3.LeapYearUtil;

/**
 * Created by User-PC on 01.07.2017.
 */
public class RealDateUtil {
	public static boolean isRealDate(int day, int month, int year) {
		boolean isReal = false;

		if (month >= 1 && month <= 12)
			if (LeapYearUtil.isLeapYear(year) && month == 2 && day >= 1 && day <= 29) {
				isReal = true;
			} else {
				int currenMonthDays;

				currenMonthDays = numberOfDaysInMonth(month);
				if (day >= 1 && day <= currenMonthDays) {
					isReal = true;
				}
			}
		return isReal;
	}

	private static int numberOfDaysInMonth(int month) {
		int currenMonthDays = 0;
		switch (month) {
			case 1:
			case 3:
			case 5:
			case 8:
			case 10:
			case 12:
				currenMonthDays = 31;
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				currenMonthDays = 30;
			case 2:
				currenMonthDays = 28;
		}
		return currenMonthDays;
	}
}
