package LessonTasks.Lesson3.Task10;

import util.UserInput;

/**
 * Created by User-PC on 02.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		int number = UserInput.inputInt("Enter number: ");
		int factorialOfNumber1 = whileFactorial(number);
		int factorialOfNumber2 = forFactorial(number);

		System.out.println("while !" + number + ": " + factorialOfNumber1);
		System.out.println("for !" + number + ": " + factorialOfNumber1);
	}

	public static int whileFactorial(int number) {
		int factorial = 1;
		int currentIterationValue = 1;
		while (currentIterationValue < number + 1) {
			factorial *= currentIterationValue;
			currentIterationValue++;
		}
		return factorial;
	}

	public static int forFactorial(int number) {
		int factorial = 1;

		for (int i = 1; i < number + 1; i++) {
			factorial *= i;
		}

		return factorial;
	}
}
