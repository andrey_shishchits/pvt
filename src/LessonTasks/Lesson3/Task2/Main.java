package LessonTasks.Lesson3.Task2;

/**
 * Created by User-PC on 01.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		int[] array = new int[10];
		int i = 0;
		int curValue = 1;

		while (i < array.length) {
			if (curValue % 13 == 0 || curValue % 17 == 0) {
				array[i] = curValue;
				i++;
			}
			curValue++;
		}
		System.out.println();

		for (int i1 = 0; i1 < array.length; i1++ ) {
			if (i1 % 10 == 0) System.out.println();

			System.out.print(array[i1] + " ");
		}
	}

}
