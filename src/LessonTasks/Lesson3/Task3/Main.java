package LessonTasks.Lesson3.Task3;

import java.util.Random;

/**
 * Created by User-PC on 01.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		Random rn = new Random();
		int[] array = new int[10];
		int range = 1000;
		int lowestElement = range + 1;
		int	highestElement = -range - 1;
		int lowestElementIndex = -1;
		int highestElementIndex = -1;
		int summ = 0;



		for (int i = 0; i < array.length; i++){
			int curElement = rn.nextInt(range) + 1;
			if (i % 2 == 0) curElement = -curElement;
			array[i] = curElement;
		}

		for (int i = 0; i < array.length; i++){
			if (i % 10 == 0) System.out.println();

			System.out.print(array[i] + " ");
		}

		for (int i = 0; i < array.length; i ++){
			if (lowestElement > array[i]){
				lowestElement = array[i];
				lowestElementIndex = i;
			}

			if (highestElement < array[i]){
				highestElement = array[i];
				highestElementIndex = i;
			}
		}

		if (highestElementIndex < lowestElementIndex) {
			highestElementIndex = highestElementIndex ^ lowestElementIndex;
			lowestElementIndex = highestElementIndex ^ lowestElementIndex;
			highestElementIndex = highestElementIndex ^ lowestElementIndex;
		}

		for (int i = lowestElementIndex + 1; i < highestElementIndex; i++){
			summ += array[i];
		}
		System.out.println();
		System.out.println("Summ of elements between lowest & highest elements: " + summ);
	}
}
