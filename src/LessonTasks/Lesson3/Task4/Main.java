package LessonTasks.Lesson3.Task4;

import java.util.Random;

/**
 * Created by User-PC on 01.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		Random rn = new Random();
		int lowestEdge = 10;
		int highestEdge = 99;
		int arrayLength = 2;
		int[] array = new int[arrayLength];
		int numberOfElements = 0;

		for (int i = 0; i < array.length; i++) {
			array[i] = rn.nextInt(highestEdge - lowestEdge) + lowestEdge;
		}

		for (int i : array) {
			System.out.print(i + " ");
		}

		for (int i = 1; i < array.length; i++) {
			if (array[i] > array[i - 1]) numberOfElements++;
		}

		if (numberOfElements == arrayLength - 1) {
			System.out.println();
			System.out.println("Array in ascending order!");
		}
	}
}
