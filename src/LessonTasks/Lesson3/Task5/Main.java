package LessonTasks.Lesson3.Task5;

import util.UserInput;

/**
 * Created by User-PC on 01.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		int arraySize = 0;

		arraySize = UserInput.inputInt("Enter array size: ");

		Butterfly butterfly = new Butterfly(arraySize);

		butterfly.printButterfly();
		System.out.println();
		butterfly.printButterflyWithStarsAndSpaces();

	}
}
