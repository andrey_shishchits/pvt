package LessonTasks.Lesson3.Task7_8;

/**
 * Created by User-PC on 02.07.2017.
 */
public class Card {
	private int value;
	private int suit;

	protected Card(int value, int suit){
		this.value = value;
		this.suit = suit;
	}

	public int getValue() {
		return value;
	}


	public int getSuit() {
		return suit;
	}

	@Override
	public String toString() {
		String value = "";
		String suit = "";

		switch (this.value) {
			case 0:
				value = "2";
				break;
			case 1:
				value = "3";
				break;
			case 2:
				value = "4";
				break;
			case 3:
				value = "5";
				break;
			case 4:
				value = "6";
				break;
			case 5:
				value = "7";
				break;
			case 6:
				value = "8";
				break;
			case 7:
				value = "9";
				break;
			case 8:
				value = "10";
				break;
			case 9:
				value = "Jack";
				break;
			case 10:
				value = "Queen";
				break;
			case 11:
				value = "King";
				break;
			case 12:
				value = "Ace";
				break;
		}

		switch (this.suit) {
			case 0:
				suit = "Hearts";
				break;
			case 1:
				suit = "Spades";
				break;
			case 2:
				suit = "Clubs";
				break;
			case 3:
				suit = "Diamonds";
				break;
		}

		return value +  " of " + suit;
	}
}
