package LessonTasks.Lesson3.Task7_8;

import java.util.Random;

/**
 * Created by User-PC on 02.07.2017.
 */
public class Deck {
	private final static int deckLength = 52;
	private final static int numberOfValues = 13;
	private final static int numberOfSuits = 4;

	private Card[] deck;

	public Deck() {
		deck = new Card[deckLength];
		int deckIterator = 0;

		for (int i = 0; i < numberOfValues; i++) {
			for (int j = 0; j < numberOfSuits; j++) {
				deck[deckIterator] = new Card(i, j);
				deckIterator++;
			}
		}
	}

	public void shuffleDeck(int count) {
		Random rn = new Random();

		for (int i = 0; i < count; i++) {
			int shuffableIndex1 = rn.nextInt(deckLength);
			int shuffableIndex2 = rn.nextInt(deckLength);
			Card card;

			card = deck[shuffableIndex1];
			deck[shuffableIndex1] = deck[shuffableIndex2];
			deck[shuffableIndex2] = card;
		}
	}

	public void printDeck() {
		for (Card card : deck) {
			System.out.println(card);
		}
	}

	public String getRandomCard() {
		Random rn = new Random();
		int random = rn.nextInt(deckLength);

		return deck[random].toString();
	}
}
