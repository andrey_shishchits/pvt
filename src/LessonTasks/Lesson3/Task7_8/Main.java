package LessonTasks.Lesson3.Task7_8;

/**
 * Created by User-PC on 02.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		Deck deck = new Deck();

		for (int i = 0; i < 10; i++){
			System.out.println(deck.getRandomCard());
		}

		deck.printDeck();

		deck.shuffleDeck(100);

		deck.printDeck();

	}
}
