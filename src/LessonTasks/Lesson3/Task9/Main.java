package LessonTasks.Lesson3.Task9;

import util.UserInput;

/**
 * Created by User-PC on 02.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		int number = UserInput.inputInt("Input number: ");

		if (isSimpleNumber(number)) {
			System.out.println("Entered number: " + number + " is sipmple");
		} else System.out.println("Entered number: " + number + " is not sipmple");
	}

	public static boolean isSimpleNumber(int number) {
		int isSimpleCounter = 0;

		for (int i = 1; i < number + 1; i++) {
			if (number % i == 0) isSimpleCounter++;
		}
		if (isSimpleCounter == 2) return true;

		return false;
	}
}
