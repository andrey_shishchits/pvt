package LessonTasks.Lesson4.Task1_1;

import util.UserInput;

/**
 * Created by User-PC on 02.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		int seconds = UserInput.inputInt("Enter amount of seconds: ");
		int hours = 2;
		int minutes = 30;
		int second = 23;

		TimeInterval timeInterval1 = new TimeInterval(seconds);
		TimeInterval timeInterval2 = new TimeInterval(hours, minutes, second);

		System.out.println(timeInterval1.getTineInterval());
		System.out.println(timeInterval2.getTineInterval());

		System.out.println(seconds);
	}
}
