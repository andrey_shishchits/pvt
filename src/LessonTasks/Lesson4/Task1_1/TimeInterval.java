package LessonTasks.Lesson4.Task1_1;

/**
 * Created by User-PC on 02.07.2017.
 */
public class TimeInterval {
	private int hours;
	private int minutes;
	private int seconds;

	private TimeInterval() {
	}

	public TimeInterval(int seconds) {
		if (seconds > 3600) {
			hours = seconds / 3600;
			seconds = seconds % 3600;
		}

		if (seconds > 60) {
			minutes = seconds / 60;
			seconds = seconds % 60;
		}

		this.seconds = seconds;
	}

	public TimeInterval(int hours, int minutes, int seconds) {
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}


	public int getTineInterval() {
		return hours * 3600 + minutes * 60 + seconds;
	}

	@Override
	public String toString() {
		return String.valueOf(this.getTineInterval());
	}
}
