package LessonTasks.Lesson4.Task1_2;

/**
 * Created by User-PC on 02.07.2017.
 */
public class Atm {
	private final static int TWENTY = 20;
	private final static int FIFTY = 50;
	private final static int HUNDRED = 100;
	private int twenties;
	private int fifties;
	private int hundreds;


	public Atm(int twenties, int fifties, int hundreds) {
		this.twenties = twenties;
		this.fifties = fifties;
		this.hundreds = hundreds;

	}

	private int countTotalAmount() {
		return twenties * TWENTY + fifties * FIFTY + hundreds * HUNDRED;
	}

	public boolean getCash(int amount) {
		boolean isPosible = false;

		if (isCorrectAmount(amount))
			if (isEnoughMoney(amount))
				if (collectBankNotes(amount)) {
					isPosible = true;
				} else isPosible = false;

		return isPosible;
	}

	private boolean isCorrectAmount(int amount) {
		boolean flag;
		if (amount % 10 == 0) {
			flag = true;
		} else {
			flag = false;
			System.out.println("Incorrect amount of money! Our ATM gives out a multiple of twenty and fifty only.");
		}

		return flag;
	}

	private boolean isEnoughMoney(int amount) {
		boolean flag;
		if (this.countTotalAmount() > amount) flag = true;
		else {
			flag = false;
			System.out.println("Incorrect amount of money! Current amount of money in ATM: " + this.countTotalAmount());
		}

		return flag;
	}

	private boolean collectBankNotes(int amount) {
		boolean flag = true;
		String msg = "";
		int requestedTwenties = 0;
		int requestedFifties = 0;
		int requestedHundreds = 0;

		if (amount % TWENTY != 0 && amount % FIFTY != 0 && amount != 0) {
			if (amount > 100) requestedHundreds = amount / HUNDRED - 1;

			if (this.hundreds < requestedHundreds) {
				requestedHundreds = this.hundreds;
				amount -= requestedHundreds * HUNDRED;
			} else {
				amount -= requestedHundreds * HUNDRED;
			}

			if (amount / FIFTY >= 1) {
				requestedFifties = amount / FIFTY;
				if (this.fifties < requestedFifties) {

					while (this.fifties < requestedFifties) {
						requestedFifties -= 2;
					}
					if (requestedFifties > 0) {
						amount -= requestedFifties * FIFTY;

						while (amount != 0) {
							requestedTwenties++;
							amount -= TWENTY;
						}
						if (this.twenties < requestedTwenties) flag = false;

					} else {
						flag = false;
						msg = "Not enough fifties in ATM.";
					}

				} else {

					while ((amount - requestedFifties * FIFTY) % 20 != 0) {
						requestedFifties -= 1;
					}
					amount -= requestedFifties * FIFTY;

					while (amount != 0) {
						requestedTwenties++;
						amount -= TWENTY;
					}
					if (this.twenties < requestedTwenties) flag = false;
				}

			} else if (amount % 20 == 0) {

				while (amount != 0) {
					requestedTwenties++;
					amount -= TWENTY;
				}
			} else {
				flag = false;
				msg = "Don't have such banknote!";
			}


		} else {
			requestedHundreds = amount / HUNDRED;
			if (requestedHundreds > this.hundreds) {
				requestedHundreds = this.hundreds;
			}
			amount -= requestedHundreds * HUNDRED;


			while(amount > 50){
				requestedFifties++;
			}
		}
		if (flag) {
			msg = "U have earned: " + requestedHundreds + " hundreds," + requestedFifties +
					" fifties and " + requestedTwenties + " tweties.";

		}
		System.out.println(msg);
		return flag;

	}

	public void addTwenties(int twenties) {
		this.twenties += twenties;
	}

	public void addFifties(int fifties) {
		this.fifties += fifties;
	}

	public void addHundreds(int hundreds) {
		this.hundreds += hundreds;
	}

	@Override
	public String toString() {
		return "Atm banknotes{" +
				"twenties=" + twenties +
				", fifties=" + fifties +
				", hundreds=" + hundreds +
				" with total amount=" + this.countTotalAmount() +
				'}';
	}
}
