package LessonTasks.Lesson5;

/**
 * Created by User-PC on 26.06.2017.
 */
public class Archer extends Hero {
	public Archer(String name, int health) {
		super(name, health);
	}

	@Override
	public void attackEnemy(Enemy enemy, int damage) {
		enemy.takeDamage(this, damage * 3);
		System.out.println("Archer attacks enemy for: " + damage * 3 + " damage!");
	}
}
