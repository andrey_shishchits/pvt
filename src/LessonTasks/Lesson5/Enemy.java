package LessonTasks.Lesson5;

/**
 * Created by User-PC on 26.06.2017.
 */
public class Enemy implements Mortal {
	private int health;

	public Enemy(int health) {
		this.health = health;
	}

	public void takeDamage(Hero hero, int damage) {
		this.health -= damage;

		hero.takeDamage(damage);
	}

	public int getHealth() {
		return this.health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	@Override
	public boolean isAlive() {
		return (health > 0);
	}
}
