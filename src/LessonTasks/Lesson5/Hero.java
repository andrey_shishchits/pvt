package LessonTasks.Lesson5;

/**
 * Created by User-PC on 26.06.2017.
 */
public abstract class Hero {
	private String name;
	private int health;

	public Hero(String name, int health) {
		this.name = name;
		this.health = health;
	}

	public abstract void attackEnemy(Enemy enemy, int damage);

	public boolean isAlive() {
		return (health > 0);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public void takeDamage(int damage) {
		this.health -= damage;
	}
}
