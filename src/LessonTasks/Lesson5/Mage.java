package LessonTasks.Lesson5;

/**
 * Created by User-PC on 26.06.2017.
 */
public class Mage extends Hero {
	public Mage(String name, int health) {
		super(name, health);
	}

	@Override
	public void attackEnemy(Enemy enemy, int damage){
		enemy.takeDamage(this,damage * 2);
		System.out.println("Mage attacks enemy 4: " + damage * 2 + " damage!");
	}

}
