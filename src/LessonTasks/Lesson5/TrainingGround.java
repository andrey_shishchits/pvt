package LessonTasks.Lesson5;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User-PC on 26.06.2017.
 */
public class TrainingGround {
	public static void main(String[] args) {
		List<Hero> heroes = new ArrayList<>();
		Zombie zombie = new Zombie(200);

		heroes.add(new Mage("Magician", 100));
		heroes.add(new Archer("Archie", 100));
		heroes.add(new Warrior("Warriorry", 100));


		for (Hero curHero : heroes) {
			curHero.attackEnemy(zombie, 40);
			System.out.println("Current zombie health: " + zombie.getHealth());
		}


	}


}
