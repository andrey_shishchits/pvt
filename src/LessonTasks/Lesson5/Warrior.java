package LessonTasks.Lesson5;

/**
 * Created by User-PC on 26.06.2017.
 */
public class Warrior extends Hero {

	public Warrior(String name, int health) {
		super(name, health);
	}

	@Override
	public void attackEnemy(Enemy enemy, int damage){
		enemy.takeDamage(this, damage);
		System.out.println("Warrior attacks enemy 4: " + damage + " damage!");
	}
}
