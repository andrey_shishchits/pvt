package LessonTasks.Lesson5;

import java.util.Random;

/**
 * Created by User-PC on 15.07.2017.
 */
public class Zombie extends Enemy {

	public Zombie(int health) {
		super(health);
	}

	@Override
	public boolean isAlive() {
		if (super.getHealth() <= 0) {
			Random rn = new Random();
			if (rn.nextInt(1) == 0) {
				this.setHealth(100);
				System.out.println("Zombie rises!");
				return true;
			} else return false;
		} else return true;
	}

	@Override
	public int getHealth(){
		if (isAlive()) {
			return super.getHealth();
		} else	{
			System.out.println("Zombie dead!");
			return 0;
		}
	}
}
