package LessonTasks.Lesson7;


import LessonTasks.Lesson7.Pair;
import LessonTasks.Lesson7.PairUtil;

/**
 * Created by User-PC on 27.06.2017.
 */
public class Main {
	public static void main(String[] args) {
		Pair<String, Integer> pair = new Pair<>("name", 2);
		System.out.println(pair);
		pair = PairUtil.swap(pair);
		System.out.println(pair);
	}
}
