package LessonTasks.Lesson7;

/**
 * Created by User-PC on 27.06.2017.
 */
public class Pair<K, V> {
	private K k;
	private V v;

	public Pair(K k, V v) {
		this.k = k;
		this.v = v;
	}

	public K getK() {
		return k;
	}

	public void setK(K k) {
		this.k = k;
	}

	public V getV() {
		return v;
	}

	public void setV(V v) {
		this.v = v;
	}

	@Override
	public String toString() {
		return "Pair{" +
				"k=" + k +
				", v=" + v +
				'}';
	}
}
