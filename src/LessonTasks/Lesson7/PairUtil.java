package LessonTasks.Lesson7;

/**
 * Created by User-PC on 27.06.2017.
 */
public final class PairUtil {
	public static Pair swap(Pair pair) {
		Object ob;

		ob = pair.getK();
		pair.setK(pair.getV());
		pair.setV(ob);

		return pair;
	}
}
