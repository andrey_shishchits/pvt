package LessonTasks.Lesson8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

/**
 * Created by User-PC on 29.06.2017.
 */
public class Main {
	public static void main(String[] args) {
		Random rn = new Random();
		List<Integer> markList = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			markList.add(rn.nextInt(10) + 1);
		}
		System.out.println(markList);

		Iterator<Integer> markListIterator = markList.iterator();

		while (markListIterator.hasNext()) {
			if (markListIterator.next() < 4) markListIterator.remove();
		}

		System.out.println(markList);

		markListIterator = markList.listIterator();

		Integer hugestMark = -1;

		while (markListIterator.hasNext()) {
			Integer currentMark = markListIterator.next();
			if (currentMark > hugestMark) {
				hugestMark = currentMark;
			}
		}

		System.out.println("Hugest mark is: " + hugestMark);

		List<String> apprenticeList = new ArrayList<>();

		apprenticeList.add("Andrey");
		apprenticeList.add("Igor");
		apprenticeList.add("Vasya");
		apprenticeList.add("Barsik");
		apprenticeList.add("Sharik");

		ListIterator<String> apprenticeListIterator = apprenticeList.listIterator();

		while (apprenticeListIterator.hasNext()) {
			System.out.print(apprenticeListIterator.next() + " ");
		}

		System.out.println();

		while (apprenticeListIterator.hasPrevious()) {
			System.out.print(apprenticeListIterator.previous() + " ");
		}

		List<Integer> numbersList = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			int currentNumber = rn.nextInt(10) - 5;

			numbersList.add(currentNumber);
		}

		System.out.println();
		System.out.println(numbersList);

		Collections.sort(numbersList);
		Collections.reverse(numbersList);

		System.out.println(numbersList);
	}
}
