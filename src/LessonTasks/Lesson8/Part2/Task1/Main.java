package LessonTasks.Lesson8.Part2.Task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * Created by User-PC on 07.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		Random rn = new Random();
		List<Integer> list = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			list.add(rn.nextInt(20));
		}
		System.out.println(countUnique(list));
	}

	public static int countUnique(List<Integer> list) {
		if (list.size() == 0) {
			return 0;
		} else return new HashSet<>(list).size();
	}
}
