package LessonTasks.Lesson8.Part2.Task2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Created by User-PC on 07.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();

		map.put("Andrey", "Shishchits");
//		map.put("Valentin", "Shishchits");
//		map.put("Pavel", "Shishchits");
		map.put("Mariya", "Kosicina");
		map.put("Valeriya", "Kosicina");

		System.out.println(isUnique(map));

	}

	private static boolean isUnique(Map<String, String> map) {
		return new HashSet<String>(map.values()).size() == map.size();

	}
}
