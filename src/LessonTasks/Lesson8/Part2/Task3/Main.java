package LessonTasks.Lesson8.Part2.Task3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by User-PC on 07.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		Map<Integer, Integer> polynomial1 = new HashMap<>();
		Map<Integer, Integer> polynomial2 = new HashMap<>();

		polynomial1.put(5, -5);
		polynomial1.put(3, 10);
		polynomial1.put(1, -15);
		polynomial1.put(0, -2);

		polynomial2.put(4, 90);
		polynomial2.put(3, -1);
		polynomial2.put(2, 40);
		polynomial2.put(1, 0);

		concatPolynomials(polynomial1, polynomial2);
	}

	public static void concatPolynomials(Map<Integer, Integer> map1, Map<Integer, Integer> map2) {
		Map<Integer, Integer> resultMap = new HashMap<>(map1);

		resultMap.putAll(map2);

		for (Map.Entry entry : resultMap.entrySet()) {
			if (map1.containsKey(entry.getKey()) && map2.containsKey(entry.getKey())) {
				entry.setValue(map1.get(entry.getKey()) + map2.get(entry.getKey()));
			}
		}
		List<Integer> keyList = new ArrayList<>(resultMap.keySet());

		Collections.sort(keyList);
		Collections.reverse(keyList);

		if (keyList.size() != 0) {
			System.out.println();
			System.out.print(resultMap.get(keyList.get(0)) + "x^" + keyList.get(0));
			for (int i = 1; i < keyList.size(); i++) {
				if (keyList.get(i) == 1 && resultMap.get(keyList.get(i)) > 0) {
					System.out.print("+" + resultMap.get(keyList.get(i)) + "x");
				} else if (keyList.get(i) == 1 && resultMap.get(keyList.get(i)) < 0) {
					System.out.print(resultMap.get(keyList.get(i)) + "x");
				} else if (keyList.get(i) == 0 && resultMap.get(keyList.get(i)) > 0) {
					System.out.print("+" + resultMap.get(keyList.get(i)));
				} else if (keyList.get(i) == 0 && resultMap.get(keyList.get(i)) < 0) {
					System.out.print(resultMap.get(keyList.get(i)) + "x");
				} else if (resultMap.get(keyList.get(i)) < 0 && resultMap.get(keyList.get(i)) != 0) {
					System.out.print(resultMap.get(keyList.get(i)) + "x^" + keyList.get(i));
				} else if (resultMap.get(keyList.get(i)) > 0 && resultMap.get(keyList.get(i)) != 0) {
					System.out.print("+" + resultMap.get(keyList.get(i)) + "x^" + keyList.get(i));
				}

			}
		}
	}
}


