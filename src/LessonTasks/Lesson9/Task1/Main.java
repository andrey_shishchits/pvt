package LessonTasks.Lesson9.Task1;

/**
 * Created by User-PC on 04.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		Integer var = null;

		try {
			var.byteValue();
		} catch (RuntimeException e) {
			System.out.println("Exception message: " + e.getMessage());
			System.out.println("Exception message: " + e.getCause());

			System.out.println("Exception stack trace: " + e.getStackTrace());
		}
	}
}
