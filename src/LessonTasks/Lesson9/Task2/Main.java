package LessonTasks.Lesson9.Task2;

/**
 * Created by User-PC on 04.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		int[] array = new int[10];

		try {
			array[10] = 5;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Exception message: " + e.getMessage());
			System.out.println("Exception message: " + e.toString());
			System.out.println("Exception stack trace: " + e.getStackTrace());
		}
	}
}
