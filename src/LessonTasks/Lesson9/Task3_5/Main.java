package LessonTasks.Lesson9.Task3_5;

/**
 * Created by User-PC on 04.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		try {
			iLikeToTrowExceptions();
		} catch (MyException e){
			System.out.println("My exception message: " + e.getMessage());
		}
	}

	public static void iLikeToTrowExceptions() throws MyException {
		Integer var = null;

		try {
			var.byteValue();
		} catch (RuntimeException e) {
			throw new MyException(e.getMessage());
		}
	}
}
