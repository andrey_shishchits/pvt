package LessonTasks.Lesson9.Task6;

import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.file.ProviderNotFoundException;
import java.util.Random;

/**
 * Created by User-PC on 04.07.2017.
 */
public class Main {
	public static void main(String[] args) {
		Random rn = new Random();
		for (int i = 0; i < 10; i++) {
			int randomValue = 0;
			try {
				randomValue = rn.nextInt(3);
				switch (randomValue) {
					case 0:
						throw new Exception();
					case 1:
						throw new RuntimeException();
					case 2:
						throw new BufferOverflowException();
				}
			} catch (BufferOverflowException e) {
				System.out.println(e.getClass() + " " + randomValue);
			} catch (RuntimeException e) {
				System.out.println(e.getClass() + " " + randomValue);
			} catch (Exception e) {
				System.out.println(e.getClass() + " " + randomValue);
			}
		}
	}
}
