package util;

import java.util.Scanner;


public final class UserInput {
	/**
	 * This method receive float number from Console.
	 * And also prints input message.
	 *
	 * @param msg
	 * @return number
	 */
	public static float inputFloat(String msg) {
		Scanner scanner = new Scanner(System.in);

		System.out.print(msg);
		float number = scanner.nextFloat();

		return number;
	}

	/**
	 * This method receive String varaible from Console.
	 * And also prints input message.
	 *
	 * @param msg
	 * @return action
	 */
	public static String inputString(String msg) {
		Scanner scanner = new Scanner(System.in);

		System.out.print(msg);
		String action = scanner.nextLine();

		return action;
	}

	/**
	 * This method recieve int value from Console.
	 * And also prints input message.
	 *
	 * @param msg
	 * @return
	 */
	public static int inputInt(String msg) {
		Scanner scanner = new Scanner(System.in);
		int number = 0;

		System.out.print(msg);
		number = scanner.nextInt();

		return number;
	}
}